# The base image
FROM ubuntu:latest

MAINTAINER Raj Gowtham <rajgowtham3893@gmail.com>
LABEL Description="Hotel Booking API"

COPY target/hotel-booking.jar /hotel-booking.jar

CMD echo Docker container started.
CMD exec java -jar /hotel-booking.jar
EXPOSE   8080