package com.raj.airasia.hotelbooking.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@Data
@ApiModel(description = "Request model doe Booking object")
public class BookingRequest {

    @ApiModelProperty(notes = "hotel id")
    private String hotelId;

    @ApiModelProperty(notes = "hotel name")
    private String hotelName;

    @ApiModelProperty(notes = "hotel checkin date")
    private String hotelCheckInDate;

    @ApiModelProperty(notes = "hotel checkout date")
    private String hotelCheckOutDate;

    @ApiModelProperty(notes = "hotel room id")
    private String hotelRoomId;

    @ApiModelProperty(notes = "hotel room name")
    private String hotelRoomName;

    @ApiModelProperty(notes = "hotel number of guests")
    private String hotelNumberOfGuests;

    @ApiModelProperty(notes = "customer name")
    private String customerName;

    @ApiModelProperty(notes = "customer email")
    private String customerEmail;

    @ApiModelProperty(notes = "customer number")
    private String customerNumber;
}
