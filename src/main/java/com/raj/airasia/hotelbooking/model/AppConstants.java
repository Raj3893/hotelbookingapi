package com.raj.airasia.hotelbooking.model;

/**
 * Constant Class
 */
public class AppConstants {

    public static final String NULL_TYPE_EXCEPTION = "Null";
    public static final String NULL_EXCEPTION_MESSAGE = "One or more values is found to be null while trying to save the object";
    public static final String REPOSITORY_TYPE_EXCEPTION = "Repository";
    public static final String REPOSITORY_EXCEPTION_MESSAGE = "Exception thrown when performing operations on repository";
    public static final String REPOSITORY_UNMODIFIED_TYPE_EXCEPTION = "RepoUnmodified";
    public static final String REPOSITORY_UNMODIFIED_EXCEPTION_MESSAGE = "Exception thrown when adding objects in repository";
    public static final String GENERAL_EXCEPTION_MESSAGE = "Unknown exception found, please check logs";
    public static final String RESPONSE_STATUS = "Booking Saved Successfully";
}
