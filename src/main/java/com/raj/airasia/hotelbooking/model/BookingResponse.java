package com.raj.airasia.hotelbooking.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@Data
@AllArgsConstructor
@ApiModel(description = "Response model for booking object")
public class BookingResponse {

    @ApiModelProperty(notes = "booking status")
    private String bookingStatus;
}
