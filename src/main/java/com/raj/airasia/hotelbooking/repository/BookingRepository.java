package com.raj.airasia.hotelbooking.repository;

import com.raj.airasia.hotelbooking.dto.BookingDto;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for Hotel booking functions
 */
public interface BookingRepository  extends CrudRepository<BookingDto, Long> {
}
