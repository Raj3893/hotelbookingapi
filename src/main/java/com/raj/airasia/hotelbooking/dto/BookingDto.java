package com.raj.airasia.hotelbooking.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "BOOKING_DETAILS")
public class BookingDto implements Serializable {

    @Id
    @Column(name = "BOOKING_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookingId;

    @OneToOne(mappedBy = "bookingDto", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private CustomerDto customerDto;

    @OneToOne(mappedBy = "bookingDto", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private HotelDto hotelDto;

    @Column(name = "BOOKING_CHECKIN")
    private Date hotelCheckInDate;

    @Column(name = "BOOKING_CHECKOUT")
    private Date hotelCheckOutDate;

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public CustomerDto getCustomerDto() {
        return customerDto;
    }

    public void setCustomerDto(CustomerDto customerDto) {
        this.customerDto = customerDto;
    }

    public HotelDto getHotelDto() {
        return hotelDto;
    }

    public void setHotelDto(HotelDto hotelDto) {
        this.hotelDto = hotelDto;
    }

    public Date getHotelCheckInDate() {
        return hotelCheckInDate;
    }

    public void setHotelCheckInDate(Date hotelCheckInDate) {
        this.hotelCheckInDate = hotelCheckInDate;
    }

    public Date getHotelCheckOutDate() {
        return hotelCheckOutDate;
    }

    public void setHotelCheckOutDate(Date hotelCheckOutDate) {
        this.hotelCheckOutDate = hotelCheckOutDate;
    }
}
