package com.raj.airasia.hotelbooking.mapper;

import com.raj.airasia.hotelbooking.dto.BookingDto;
import com.raj.airasia.hotelbooking.dto.CustomerDto;
import com.raj.airasia.hotelbooking.dto.HotelDto;
import com.raj.airasia.hotelbooking.model.BookingRequest;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Mapper class
 */
public class BookingMapper {

    /**
     * Used to convert request to dto object
     *
     * @param bookingRequest
     *     booking request object
     * @return booking dto object
     */
    public static BookingDto convertRequestToDto(BookingRequest bookingRequest) {
        BookingDto bookingDto = new BookingDto();
        HotelDto hotelDto = new HotelDto();
        CustomerDto customerDto = new CustomerDto();
        customerDto.setBookingDto(bookingDto);
        customerDto.setEmail(bookingRequest.getCustomerEmail());
        customerDto.setName(bookingRequest.getCustomerName());
        customerDto.setPhoneNumber(bookingRequest.getCustomerNumber());
        hotelDto.setId(Long.parseLong(bookingRequest.getHotelId()));
        hotelDto.setHotelName(bookingRequest.getHotelName());
        hotelDto.setHotelRoomName(bookingRequest.getHotelRoomName());
        hotelDto.setHotelRoomId(bookingRequest.getHotelRoomId());
        hotelDto.setBookingDto(bookingDto);
        bookingDto.setCustomerDto(customerDto);
        bookingDto.setHotelDto(hotelDto);
        bookingDto.setHotelCheckInDate(parseDateFromString(bookingRequest.getHotelCheckInDate()));
        bookingDto.setHotelCheckOutDate(parseDateFromString(bookingRequest.getHotelCheckOutDate()));
        return bookingDto;
    }

    /**
     * Function to parse date from string
     *
     * @param dateString
     *     date string value
     * @return date
     */
    private static Date parseDateFromString(String dateString) {
        long date=new SimpleDateFormat("dd-MM-yyyy").parse(dateString, new ParsePosition(0)).getTime();
        return new java.sql.Date(date);
    }
}