package com.raj.airasia.hotelbooking.controller;

import com.raj.airasia.hotelbooking.exception.ServiceException;
import com.raj.airasia.hotelbooking.model.BookingRequest;
import com.raj.airasia.hotelbooking.model.BookingResponse;
import com.raj.airasia.hotelbooking.model.ErrorDetails;
import com.raj.airasia.hotelbooking.service.BookingService;
import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class
 */
@RestController
@RequestMapping("/v1")
@Api(value = "HotelBooking API", description = "An API that creates, updates hotel booking details data")
public class BookingController {

    Logger logger = LoggerFactory.getLogger(BookingController.class);

    /** Service object */
    @Autowired
    private BookingService bookingService;

    /**
     * Performs an action to save the object
     *
     * @param bookingRequest
     *     booking request object
     * @return status code of 200
     * @throws ServiceException
     *     Service Exception thrown
     */
    @ApiOperation(value = "Saves an object of hotel booking details of a user in the database",
            notes = "Saves an object of hotel booking details of a user in the database", tags = {})
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully saved the booking object"),
            @ApiResponse(code = 304, message = "Resource was not updated", response = ErrorDetails.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ErrorDetails.class),
            @ApiResponse(code = 304, message = "Resource you are trying to reach is not found/available", response = ErrorDetails.class)})
    @RequestMapping(value = "/saveBookingDetails", produces = {"application/json"}, method = RequestMethod.POST)
    @Timed
    public ResponseEntity<BookingResponse> saveBookingDetails(@RequestBody BookingRequest bookingRequest) throws ServiceException {
        BookingResponse bookingResponse = this.bookingService.saveBooking(bookingRequest);
        return new ResponseEntity<BookingResponse>(bookingResponse, HttpStatus.OK);
    }
}