package com.raj.airasia.hotelbooking.service;

import com.raj.airasia.hotelbooking.exception.ServiceException;
import com.raj.airasia.hotelbooking.model.BookingRequest;
import com.raj.airasia.hotelbooking.model.BookingResponse;

/**
 * Serviec Implementation
 */
public interface BookingService {

    /**
     * function to save the booking request object
     *
     * @param bookingRequest
     *     booking request object
     * @return {@link BookingResponse}
     *
     * @throws ServiceException
     *     throws service exception
     */
    BookingResponse saveBooking(BookingRequest bookingRequest) throws ServiceException;
}