package com.raj.airasia.hotelbooking.service.implementations;

import com.raj.airasia.hotelbooking.dto.BookingDto;
import com.raj.airasia.hotelbooking.exception.ServiceException;
import com.raj.airasia.hotelbooking.mapper.BookingMapper;
import com.raj.airasia.hotelbooking.model.AppConstants;
import com.raj.airasia.hotelbooking.model.BookingRequest;
import com.raj.airasia.hotelbooking.model.BookingResponse;
import com.raj.airasia.hotelbooking.repository.BookingRepository;
import com.raj.airasia.hotelbooking.service.BookingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookingServiceImpl implements BookingService {

    Logger logger = LoggerFactory.getLogger(BookingServiceImpl.class);

    /** Booking Repository */
    @Autowired
    private BookingRepository bookingRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public BookingResponse saveBooking(BookingRequest bookingRequest) throws ServiceException {
        if(!nullCheck(bookingRequest)) {
            logger.error(String.format(AppConstants.NULL_EXCEPTION_MESSAGE));
            throw new ServiceException(AppConstants.NULL_TYPE_EXCEPTION);
        } else {
            BookingDto bookingDto = BookingMapper.convertRequestToDto(bookingRequest);
            try{
                this.bookingRepository.save(bookingDto);
                return new BookingResponse(AppConstants.RESPONSE_STATUS);
            } catch (Exception e) {
                logger.error(e.getMessage());
                throw new ServiceException(AppConstants.REPOSITORY_UNMODIFIED_TYPE_EXCEPTION);
            }
        }
    }

    /**
     * Method used to check for null values in the booking object
     *
     * @param bookingRequest
     *     booking request object
     * @return boolean type
     */
    private boolean nullCheck(BookingRequest bookingRequest) {
        boolean nullCheck = true;
        if(bookingRequest.getHotelId() == null) {
            nullCheck = false;
        }
        return nullCheck;
    }
}
